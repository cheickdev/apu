import java.util.*;

/**
 * The machines are gaining ground. Time to show them what we're really made of...
 **/
class Player {

    static final char LINE_CHAR = '.';
    static final char ZERO_CHAR = '0';

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        Matrix matrix = createMatrixFromInput(in);

        matrix.computeAllNeighbours();

        matrix.computeLinks();

        //matrix.printLinks();

        matrix.debug();
    }

    static Matrix createMatrixFromInput(Scanner in) {
        int width = in.nextInt(); // the number of cells on the X axis
        int height = in.nextInt(); // the number of cells on the Y axis
        System.err.println(height + " " + width);
        in.nextLine();
        Matrix matrix = new Matrix(height, width);

        for (int x = 0; x < height; x++) {
            String line = in.nextLine(); // width characters, each either a number or a '.'
            for (int y = 0; y < line.length(); y++) {
                matrix.setNode(x, y, line.charAt(y));
            }
            System.err.println(line);
        }
        return matrix;
    }

    static class Matrix {
        int width;
        int height;
        Node[][] nodes;
        LinkedList<Node> chargedNodes;
        List<Link> links;

        public Matrix(int width, int height) {
            this.width = width;
            this.height = height;
            this.nodes = new Node[width][height];
            this.chargedNodes = new LinkedList<>();
            this.links = new ArrayList<>();
            initializeNodes();
        }

        void initializeNodes() {
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    nodes[x][y] = new Node(x, y);
                }
            }
        }

        public void setNode(int x, int y, char c) {
            if (c != LINE_CHAR) {
                nodes[x][y].setCharge(c - ZERO_CHAR);
            } else {
                nodes[x][y].setCharge(0);
            }
            nodes[x][y].type = c;
        }

        public void sortNodes() {
            Collections.sort(chargedNodes);
        }

        public void computeLinks() {
            sortNodes();
            if (chargedNodes.isEmpty()) {
                return;
            }
            computeAllNeighbours();
            sortNodes();
            Node current = chargedNodes.get(0);
            int count = width * height;

            while (!chargedNodes.isEmpty() && count > 0) {
                //System.err.println(current.remain + " " + current);
                if (current.neighbours.isEmpty()) {
                    chargedNodes.remove(current);
                    count--;
                    System.err.println(chargedNodes);
                    continue;
                }
                //Node neighbour = current.neighbours.get(0);

                Collections.sort(current.neighbours, new Comparator<Node>() {
                    @Override
                    public int compare(Node o1, Node o2) {
                        return -o1.compareTo(o2);
                    }
                });
                for (int i = 0; i < 1; i++) {
                    Node neighbour = current.neighbours.get(i);

                    createLink(current, neighbour);
                }
                computeAllNeighbours();
                sortNodes();
                if (!chargedNodes.isEmpty()) {
                    current = chargedNodes.get(0);
                }
            }
        }

        void createLink(Node current, Node neighbour) {
            Link link = getLinkIfExistOfCreateAndAddToLinks(current, neighbour);
            link.update();
        }

        Link getLinkIfExistOfCreateAndAddToLinks(Node current, Node neighbour) {
            Link link = new Link(current, neighbour);
            int linkIndex = links.indexOf(link);
            if (linkIndex >= 0) {
                return links.get(linkIndex);
            }
            links.add(link);
            return link;
        }

        public void printLinks() {
            for (Link link : links) {
                System.out.println(link);
            }
        }

        boolean isInMap(int x, int y) {
            return x >= 0 && x < width && y >= 0 && y < height;
        }

        void computeAllNeighbours() {
            chargedNodes.clear();
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    if (nodes[x][y].isActive()) {
                        nodes[x][y].neighbours.clear();
                        computeNodeNeighbours(nodes[x][y]);
                        if (!nodes[x][y].neighbours.isEmpty() && nodes[x][y].isActive()) {
                            chargedNodes.add(nodes[x][y]);
                        }
                    }
                }
            }
        }

        void computeNodeNeighbours(Node node) {
            computeVerticalNeighbours(node);
            computeHorizontalNeighbours(node);
        }

        void computeVerticalNeighbours(Node node) {
            computeNeighbourOnTop(node);
            computeNeighbourOnBottom(node);
        }

        void computeHorizontalNeighbours(Node node) {
            computeNeighbourOnLeft(node);
            computeNeighbourOnRight(node);
        }

        boolean isFull(Node node, Node neighbour) {
            Link link = new Link(node, neighbour);
            int linkIndex = links.indexOf(link);
            if (linkIndex >= 0) {
                link = links.get(linkIndex);
                if (link.isSatured()) {
                    return true;
                }
            }
            return false;
        }

        void computeNeighbourOnLeft(Node node) {
            int x = node.x;
            int y = node.y;
            for (int i = 1; i <= x; i++) {
                if (isInMap(x - i, y)) {
                    Node neighbour = nodes[x - i][y];
                    if (!neighbour.isLink()) {
                        if (neighbour.isActive()) {
                            if (!isFull(node, neighbour)) {
                                node.addNeighbour(neighbour);
                            }
                        }
                        break;
                    }
                }
            }
        }

        void computeNeighbourOnRight(Node node) {
            int x = node.x;
            int y = node.y;
            for (int i = 1; x + i < width; i++) {
                if (isInMap(x + i, y)) {
                    Node neighbour = nodes[x + i][y];
                    if (!neighbour.isLink()) {
                        if (neighbour.isActive()) {
                            if (!isFull(node, neighbour)) {
                                node.addNeighbour(neighbour);
                            }
                        }
                        break;
                    }
                }
            }
        }

        void computeNeighbourOnTop(Node node) {
            int x = node.x;
            int y = node.y;
            for (int i = 1; i <= y; i++) {
                if (isInMap(x, y - i)) {
                    Node neighbour = nodes[x][y - i];
                    if (!neighbour.isLink()) {
                        if (neighbour.isActive()) {
                            if (!isFull(node, neighbour)) {
                                node.addNeighbour(neighbour);
                            }
                        }
                        break;
                    }
                }
            }
        }

        void computeNeighbourOnBottom(Node node) {
            int x = node.x;
            int y = node.y;
            for (int i = 1; y + i < height; i++) {
                if (isInMap(x, y + i)) {
                    Node neighbour = nodes[x][y + i];
                    if (!neighbour.isLink()) {
                        if (neighbour.isActive()) {
                            if (!isFull(node, neighbour)) {
                                node.addNeighbour(neighbour);
                            }
                        }
                        break;
                    }
                }
            }
        }

        public void debug() {
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    if (nodes[x][y].isActive()) {
                        nodes[x][y].debug();
                    }
                }
            }
        }
    }

    static class Link {
        Node start;
        Node end;
        int numberOfCable;

        public Link(Node start, Node end) {
            this.start = start;
            this.end = end;
            this.numberOfCable = 0;
        }

        boolean isSatured() {
            return numberOfCable == 2 || start.remain == 0 || end.remain == 0;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Link link = (Link) o;

            if (start.equals(link.start) && end.equals(link.end)) {
                return true;
            }
            if (start.equals(link.end) && end.equals(link.start)) {
                return true;
            }
            return false;

        }

        @Override
        public int hashCode() {
            int result = start != null ? start.hashCode() : 0;
            result = 31 * result + (end != null ? end.hashCode() : 0);
            int result1 = end != null ? end.hashCode() : 0;
            result1 = 31 * result1 + (start != null ? start.hashCode() : 0);
            return result + result1;
        }

        public void update() {
            start.consumeCharge();
            end.consumeCharge();
            numberOfCable++;
            System.out.println(start + " " + end + " 1");
        }

        @Override
        public String toString() {
            return start + " " + end + " " + numberOfCable;
        }
    }

    static class Node implements Comparable<Node> {
        int x;
        int y;
        int charge;
        int remain;
        char type;
        ArrayList<Node> neighbours;

        public Node(int x, int y) {
            this.x = x;
            this.y = y;
            this.neighbours = new ArrayList<>();
        }

        public boolean isActive() {
            return remain > 0;
        }

        public boolean isLink() {
            return type == '.';
        }

        @Override
        public int compareTo(Node o) {
            /* Descendant */
            int asc = -1;
            if (remain < o.remain) {
                return asc;
            }
            if (remain > o.remain) {
                return -asc;
            }
            return 0;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Node node = (Node) o;

            if (x != node.x) return false;
            return y == node.y;

        }

        @Override
        public int hashCode() {
            int result = x;
            result = 31 * result + y;
            return result;
        }

        public void addNeighbour(Node neighbour) {
            if (neighbours.contains(neighbour)) {
                return;
            }
            neighbours.add(neighbour);
        }

        void debug() {
            System.err.println("[" + this + "] --> " + neighbours);
        }

        @Override
        public String toString() {
            return y + " " + x;
        }

        public void consumeCharge() {
            remain--;
        }

        public void setCharge(int charge) {
            this.charge = charge;
            this.remain = charge;
        }
    }
}