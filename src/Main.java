public class Main {

    public static void main(String[] args) {
        Player.Node node00 = new Player.Node(0, 0);
        Player.Node node22 = new Player.Node(2, 2);
        Player.Link link0022 = new Player.Link(node00, node22);
        Player.Link link2200 = new Player.Link(node22, node00);
        System.out.println(link0022.equals(link2200));
        System.out.println(link2200.equals(link0022));
        System.out.println(link0022.hashCode());
        System.out.println(link2200.hashCode());
    }
}
